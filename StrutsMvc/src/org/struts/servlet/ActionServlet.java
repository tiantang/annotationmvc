package org.struts.servlet;

import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.struts.action.Action;
import org.struts.from.ActionFrom;
import org.struts.from.Fullfrom;
import org.struts.from.XmlBean;

public class ActionServlet extends HttpServlet{

	public void doGet(HttpServletRequest request,HttpServletResponse response){
		
		String path=this.getPath(request.getServletPath());
		System.out.println(path);
System.out.println(path.replace("//",""));
		Map<String,XmlBean> map=(Map<String,XmlBean>)this.getServletContext().getAttribute("struts");
		XmlBean xml=map.get(path.replace("//",""));
		String formclass=xml.getFormclass();
		ActionFrom from=Fullfrom.full(formclass, request);
		String actionclass=xml.getActionClass();
		Action action=null;
		String url="";
		try{
			Class cl=Class.forName(actionclass);
			action=(Action)cl.newInstance();
			url=action.execute(request, from, xml.getActionForward());
			RequestDispatcher dis=request.getRequestDispatcher(url);
			dis.forward(request, response);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	public void doPost(HttpServletRequest request,HttpServletResponse response){
		this.doGet(request, response);
	}
	
	
	public String getPath(String path){
		return path.split("\\.")[0];
	}
}
