package org.struts.from;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

public class Fullfrom {

	public Fullfrom(){
		
	}
	
	public static ActionFrom full(String formclass,HttpServletRequest request){
		ActionFrom from=null;
		try{
			System.out.println(formclass);
			Class cal=Class.forName(formclass);
			from=(ActionFrom)cal.newInstance();
			Field [] filed=cal.getDeclaredFields();
			for (Field f:filed){
				f.setAccessible(true);
				f.set(from, request.getParameter(f.getName()));
				f.setAccessible(false);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return from;
		
	}
}
