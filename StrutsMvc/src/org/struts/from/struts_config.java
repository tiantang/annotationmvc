package org.struts.from;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class struts_config {

	public struts_config() {

	}

	public static Map<String, XmlBean> getXmlbean(String path)
			throws JDOMException, IOException {
		Map<String, XmlBean> map = new HashMap<String, XmlBean>();
		SAXBuilder saxb = new SAXBuilder();
		Document dom = saxb.build(new File(path));
		Element root = dom.getRootElement();
		Element elaction = root.getChild("action-mapping");
		List<Element> list = elaction.getChildren();
		for (Element e : list) {
			XmlBean xml = new XmlBean();
			String name = e.getAttributeValue("name");
			xml.setBeanName(name);
			String type = e.getAttributeValue("type");
			xml.setActionClass(type);
			String actionpath = e.getAttributeValue("path");
			xml.setPath(actionpath);
			Element elform = root.getChild("formbeans");
			List<Element> list2 = elform.getChildren();
			for (Element e2 : list2) {
				String formname = e2.getAttributeValue("name");
				if (formname.equals(name)) {
					String formclass = e2.getAttributeValue("class");
					xml.setFormclass(formclass);
					break;
				}
			}
			List<Element> list3 = e.getChildren();
			Map<String, String> map1 = new HashMap<String, String>();
			for (Element e3 : list3) {
				String resultname = e3.getAttributeValue("name");
				String value = e3.getAttributeValue("value");
				map1.put(resultname, value);
			}
			xml.setActionForward(map1);
           map.put(actionpath, xml);
		}
		
		return map;

	}
}
