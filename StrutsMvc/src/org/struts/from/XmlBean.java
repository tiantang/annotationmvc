package org.struts.from;

import java.util.HashMap;
import java.util.Map;

public class XmlBean {

	public XmlBean(){
		
	}
	
	private String beanName="";
	
	private String path="";
	
	private String actionType="";
	
	private String actionClass="";
	
	private String formclass="";
	
	private Map<String,String> actionForward=new HashMap<String,String>();

	public String getBeanName() {
		return beanName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getActionClass() {
		return actionClass;
	}

	public void setActionClass(String actionClass) {
		this.actionClass = actionClass;
	}

	public String getFormclass() {
		return formclass;
	}

	public void setFormclass(String formclass) {
		this.formclass = formclass;
	}

	public Map<String, String> getActionForward() {
		return actionForward;
	}

	public void setActionForward(Map<String, String> actionForward) {
		this.actionForward = actionForward;
	}
	
	
	
	
}
