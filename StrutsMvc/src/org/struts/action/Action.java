package org.struts.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.struts.from.ActionFrom;

public interface Action {

	String execute(HttpServletRequest request,ActionFrom from,Map<String,String> map);
	
}
